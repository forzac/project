<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\RegForm;
use app\models\User;
use yii\data\SqlDataProvider;
use yii\web\ForbiddenHttpException;

/**
 * Class SiteController
 */
class SiteController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionUser()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else if (Yii::$app->user->can('user')) { 
            $count = Yii::$app->db->createCommand('
                            SELECT COUNT(*) FROM user 
                            ')->queryScalar();
            
            $provider = new SqlDataProvider([
                'sql' => 'SELECT id, username, email, password_hash FROM user',
                'totalCount' => $count,
                'pagination' => [
                    'pageSize' => 3,
                ],
                'sort' => [
                    'attributes' => [
                        'id',
                        'username',
                        'email',
                        'password_hash',
                    ],
                ],
            ]);
        return $this->render('user', [
            'provider' => $provider,
        ]);
        } else {
            throw new ForbiddenHttpException('У вас недостаточно прав для выполнения этого действия.');
        }    
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'foreColor' => 0x33CC33,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        //if (Yii::$app->user->can('user')) {
            return $this->render('index');
        //} else {
          //  throw new ForbiddenHttpException('Доступ запрешен.');
       // }
    }

    public function actionReg()
    {
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        $model = new RegForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()):
            if ($user = $model->reg()):
                if ($user->status === User::STATUS_ACTIVE):
                    if (Yii::$app->getUser()->login($user)):
                        return $this->goHome();
                    endif;
                endif;
            else:
                Yii::$app->session->setFlash('error', 'Возникла ошибка при регистрации.');
                Yii::error('Ошибка при регистрации');
                return $this->refresh();
            endif;
        endif;
        return $this->render('reg', [
            'model' => $model
        ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionSample()
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $text = $data['text'];
            $text2 = $data['text2'];
            $text3 = $data['text3']; //can store in mass
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'load' => $text,
                'load2' => $text2,
                'load3' => $text3
            ];
        }
    }
}
