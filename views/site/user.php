<?php

use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;

echo '<h1>Пользователи</h1>';
Pjax::begin();
echo GridView::widget([
    'dataProvider' => $provider,
]);
Pjax::end();


