<?php

use yii\bootstrap\Carousel;

echo Carousel::widget([
    'items' => [
        '<img src="/images/frontend/2.jpeg"/>',
        '<img src="/images/frontend/1.jpeg"/>',
    ]
]);
?>

<div id="load">
    <div id="result">
        <img src="/images/frontend/scrolldown.gif" alt="scroll down" id="scroll-down"/>
        <div class="name"></div>
        <div class="img-first"></div> 
        <div class="description"></div>
    </div>
</div>
