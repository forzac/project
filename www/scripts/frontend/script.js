$('document').ready(function () {
    var a = "<img src='/images/frontend/iphone1.png' alt='a'/>"; 
    var c = "<p>iPhone 6 не просто больше. Он лучше во всех отношениях. Больше, но при этом \n\
            значительно тоньше. Мощнее, но при этом исключительно экономичный. Его гладкая металлическая \n\
            поверхность плавно переходит в стекло нового HD-дисплея Retina, образуя цельный, законченный дизайн.\n\
            Его аппаратная часть идеально работает с программным обеспечением. Это новое поколение \n\
            iPhone, улучшенное во всём. </p>";
    var b = "<h1>iPhone 6</h1><span>Больше. Во всех проявлениях.</span>"; //можно хранить в json

    $(function () {
        var load = $('#result');
        var loadStatus = true;
        $(window).scroll(function () {
            if ($(window).scrollTop() > (load.position().top - $(window).height()) && loadStatus) {
                $('#scroll-down').remove();
                loadStatus = false;
                $.ajax({
                    url: '/index.php?r=site%2Fsample',
                    type: 'post',
                    data: {text: a, text2: b, text3: c},
                    success: function (data) {
                        $('#result > .img-first').append(data.load);
                        $('#result > .name').append(data.load2);
                        $("#result > .name").animate({margin: 0}, 500);
                        $("#result > .img-first").animate({margin: 0}, 500);
                        $('#result > .description').append(data.load3);
                        function showin() {
                            $('#result > .description').fadeIn(500)
                        }
                        setTimeout(showin, 600);
                    }
                });
            }
        });
    });
});


 