<?php

/**
 * Created by Denis G.
 */

namespace app\models;

use yii\base\Model;
use Yii;

/**
 * Class SiteController
 */
class RegForm extends Model
{
    /**
     * @var string
     */
    public $username;
    
    /**
     * @var string
     */
    public $email;
    
    /**
     * @var string
     */
    public $password;
    
    /**
     * @var string
     */
    public $repeat_password;
    
    /**
     * @var int
     */
    public $status;
    public $verifyCode;

    public function rules()
    {
        return [
            [['username', 'email', 'password','repeat_password'], 'filter', 'filter' => 'trim'],
            [['username', 'email', 'password', 'verifyCode', 'repeat_password'], 'required'],
            ['username', 'match', 'pattern' => '/^([a-zA-Z0-9_])+$/', 'message' => 'Только латиница и цыфры 0-9'],
            ['repeat_password', 'compare', 'compareAttribute' => 'password'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['password', 'string', 'min' => 6, 'max' => 255],
            ['username', 'unique',
                'targetClass' => User::className(),
                'message' => 'Это имя уже занято.'],
            ['email', 'email'],
            ['email', 'unique',
                'targetClass' => User::className(),
                'message' => 'Эта почта уже занята.'],
            ['status', 'default', 'value' => User::STATUS_ACTIVE, 'on' => 'default'],
            ['status', 'in', 'range' => [
                    User::STATUS_NOT_ACTIVE,
                    User::STATUS_ACTIVE
                ]],
            ['verifyCode', 'captcha','captchaAction'=>'site/captcha'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'email' => 'Эл. почта',
            'password' => 'Пароль',
            'verifyCode' => 'Код с картинки',
            'repeat_password' => 'Пароль(повтор)'
        ];
    }
    
    /**
     * Регистрация пользователей
     */
    public function reg()
    {
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->status = $this->status;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->save(false);
        $auth = Yii::$app->authManager;
        $userRole = $auth->getRole('user');
        $auth->assign($userRole, $user->getId());
        return $user;
    }
}
